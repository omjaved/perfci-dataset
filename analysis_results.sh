#!/bin/bash


# Function to check commands
function checkCommand(){
        _=$(command -v $1);
        if [ "$?" != "0" ]; then
                echo $?
                printf -- '\033[31m ERROR: You dont seem to have %s installed \033[0m\n';
                exit 127;
        fi;
}

checkCommand virtualenv
checkCommand git
checkCommand pip

## Setting up virtual environment
echo "Setting up virtual environment"
virtualenv venv --python=python2.7
source venv/bin/activate

pip install -r requirements.txt

## This scripts downloads CI data from the project.
printf -- '\033[31m Generating plots from the dataset with Analysis library  \033[0m\n';


# Download VyPR Analysis library and its dependencies
git clone https://github.com/pyvypr/VyPR.git
cd VyPR
git checkout 4f6461adfe14ca5ffcebcf5237122c781ed067e1
cd ..

git clone https://github.com/pyvypr/VyPRServer.git
cp -rf VyPR VyPRServer

cd VyPRServer
git checkout 8ef27c2db0ff234800c520e8741ca931dbfdf3a8
cd ..

git clone https://github.com/pyvypr/VyPRAnalysis.git
cd VyPRAnalysis
git checkout ad6c4e043772d35cda6bd8e641bd5f43ac5e2dba
cd ..


# Run our analysis script
python unit_test_analysis.py


# Cleanup files
rm -rf log* VyPR* venv
mv *.pdf data

