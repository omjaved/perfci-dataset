import matplotlib
matplotlib.use('agg')


import VyPRAnalysis as va
import pandas as pd
import matplotlib.pyplot as plt
import os



"""

    Script for generating plot by using data from VyPR Analysis Library

"""

"""Initialize db and set monitoring service"""

va.prepare(os.getcwd()+'/data/verdicts.db', logging=True)



"""dictionary of property"""
data_property = {}

"""Dictionary containing unit test name and range"""


def generated_dataframes():

    list_name = []
    list_range = []

    verdict_data = {}

    list_dataframes = []

    """Getting function calls unit test calls"""
    metadata_unit_tests = va.list_test_data()
    metadata_function = va.list_functions()[0]


    for unit_test in metadata_unit_tests:

        from dateutil import parser

        start_datetime = parser.parse(unit_test.start_time)

        end_datetime = parser.parse(unit_test.end_time)

        list_name.append(unit_test.test_name)

        range = (end_datetime - start_datetime).total_seconds()

        list_range.append(range)

        found_property = 0
        for function_call in unit_test.get_function_calls():
            found_property = found_property + 1
            break

        data_property[unit_test.test_name] = found_property

#
    df = pd.DataFrame(list(zip(list_name, list_range)),
                  columns =['Name', 'val'])


    unit_test_time = df.groupby(['Name'])['val'].mean().reset_index()
    unit_test_time = unit_test_time.sort_values(by=['Name'])

    property = pd.DataFrame(data_property.items(),
                            columns =['test_name', 'property'])



    property = property.sort_values(by=['test_name'])



    calls = metadata_function.get_calls()
    n_of_calls = len(calls)

    for (n, call) in enumerate(calls):


        observations = call.get_observations()

        end_time = parser.parse(eval(observations[1].observed_value)['time'])

        start_time = parser.parse(eval(observations[0].observed_value)['time'])

        measuretime = (end_time - start_time).total_seconds()

        abs_diff = abs(1 - measuretime)

        verdicts = call.get_verdicts()
        for verdict in verdicts:
            if verdict.verdict == 0:
                verdict_data[n] = -1 * abs_diff
            else:
                verdict_data[n] = 1 * abs_diff

    verdict_df = pd.DataFrame(verdict_data.items(),
                  columns =['call#', 'verdict'])


    # Storing dataframes in a list
    list_dataframes.append(unit_test_time)
    list_dataframes.append(property)
    list_dataframes.append(verdict_df)


    return list_dataframes



def figure_1_plot(dataframes):

    unit_test_time = dataframes[0]
    property = dataframes[1]
    verdict = dataframes[2]


    #plt.subplot(211)
    plt.subplot(221)

    plt.bar(unit_test_time['Name'],unit_test_time['val'],color = 'white',hatch='/',edgecolor='black',width=0.7, label="execution time")
    plt.legend(loc="upper left")
    #plt.yscale('log')
    plt.xticks([])
    plt.yticks(fontsize=10)
    plt.xticks(fontsize=10)
    plt.xlabel('Unit tests', fontsize=10)
    plt.ylabel('Avg. Execution time (sec)', fontsize=10)
    plt.legend(loc='best',ncol=2, fontsize=10)
    axes = plt.gca()
    length_tick = len(axes.get_xticklabels())
    #list = list(range(1,length_tick+1))


    #ax = plt.subplot(212)
    ax = plt.subplot(223)

    plt.bar(property['test_name'],property['property'],color = '#333333',edgecolor='black',width=0.7, label = "executed")
    #plt.yscale('log')
    plt.xticks([])
    plt.yticks([0,1])
    plt.yticks(rotation=45)
    ax.set_yticklabels(['Not Executed','Executed'])
    plt.yticks(fontsize=10)
    plt.xticks(fontsize=10)
    plt.xlabel('Unit tests', fontsize=10)
    plt.ylabel('Monitored function', fontsize=10)
    plt.legend(loc='best',ncol=2, fontsize=10)
    axes = plt.gca()


    ax = plt.subplot(122)

    plt.plot(verdict['verdict'], color='blue', drawstyle='steps-pre')


    # Give x axis label for the square wave plot

    plt.xlabel('Monitored function executed (#)')



    # Give y axis label for the square wave plot

    plt.ylabel('Verdict Severity')

    #plot.grid(True, which='both')

    # Provide x axis and line color

    #plot.axhline(y=0, color='k')



    # Set the max and min values for y axis

    plt.yticks([-12,1])


    plt.tight_layout()
    plt.savefig("ut_exec.pdf", bbox_inches='tight')


def figure_3_plot(data_property):

    ## Plot for showing optimization
    from get_data_db import get_unit_test_exection

    nonoptimized_df = get_unit_test_exection('data/non-optimize.db')

    optimized_df = get_unit_test_exection('data/optimize.db')

    keys = [name for name in data_property if data_property[name] == 1]

    nonoptimized_df = nonoptimized_df.loc[nonoptimized_df['test_name'].isin(keys)]
    optimized_df = optimized_df.loc[optimized_df['test_name'].isin(keys)]


    fig = plt.figure()

    ax = fig.add_subplot(111) # Create matplotlib axes


    width = 0.2

    plt.yscale('log')

    nonoptimized_df.execution_time.plot(kind='bar', color = 'white',edgecolor='black',hatch="/",  ax=ax, width=width, position=1, label= 'Unoptimized')
    optimized_df.execution_time.plot(kind='bar', color='black', ax=ax, width=width, position=0, label="Optimized")
    plt.legend(loc="upper left")
    ax.set_ylabel('Avg. execution time')
    ax.set_xlabel('Unit tests')
    plt.xticks([])
    plt.savefig("reduction_ut_exection.pdf", bbox_inches='tight')






if __name__ == '__main__':

     """Generating dataframes from analysis library"""
     plot_dataframes = generated_dataframes()

     """Plotting two-bars demonstrating unit test execution time, and the monitoring function over the execution of unit test"""
     figure_1_plot(plot_dataframes)


     """Plotting optimized execution of unit tests"""
     figure_3_plot(data_property)

     va.teardown()
