### Dataset for PerfCI analysis

This dataset contains performance testing done on CMS uploader service by PerfCI toolchain. 


## Dataset

There are three database files in the data directory 1) `verdicts.db` contains the analysis of VyPR in CI 2) `non-optimized.db` database contains execution time of units 3) `optimized.db` database contains execution time of unit tests after code improvement 


## Generate data

Run the script `./analysis_results.sh` to fetch data from the database and generate plots in pdf. These will be placed in `data` directory
