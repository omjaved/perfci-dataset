import sqlite3
import pandas as pd

#database_path = 'VyPRServer/non-optimize.db'

def get_unit_test_exection(database_path):

	try:
		sqliteConnection = sqlite3.connect(database_path)
		cursor = sqliteConnection.cursor()
		print("Database created and Successfully Connected to SQLite")

		sqlite_select_Query = "select test_func_name ,avg(execution_time) from instrument group by test_func_name;"
		cursor.execute(sqlite_select_Query)
		record = cursor.fetchall()
		df = pd.DataFrame(record, columns=['test_name', 'execution_time'])
		cursor.close()

	except sqlite3.Error as error:
		print("Error while connecting to sqlite", error)
	finally:
		if (sqliteConnection):
			sqliteConnection.close()
			print("The SQLite connection is closed")


	return df
